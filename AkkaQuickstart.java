package com.lightbend.akka.sample;

/**
 * 1 - implement a crash message to be sent at random
 * 2 -
 * **/

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class AkkaQuickstart {

    static ActorList actorList = new ActorList ();

    public static void main(String[] args) {

        final ActorSystem system = ActorSystem.create("system");

        final LoggingAdapter log = Logging.getLogger(system, "main");

        int n = 6;

        for (int i = 0; i < n ; i++){
            ActorList.l.add(system.actorOf(Actor.createActor(), String.valueOf("A"+i)));

        }

        for (int i = 0; i < n ; i++){
            log.info("List has been sent to A" + i );
            ActorList.l.get(i).tell(actorList, ActorRef.noSender());
        }



        log.info(ActorList.l.toString());


        sleepFor(1);

        //TODO: Find a way to call the function write hahahaha
        for (int i = 0; i < n ; i++){
            log.info("A0 writes to A" + i );
            log.info(String.valueOf(Actor.write("hello", ActorList.l.get(0))));
        }

        sleepFor(1);

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
            System.out.println(Actor.ack_list);
            System.out.println(String.valueOf(Actor.counter+" welcome message sent."));
        }
    }

    // function used to send the pair from the actor.write function
    // not done
    public static void send(ActorRef actorRef, Actor.Pair pair){

        actorRef.tell(pair, ActorRef.noSender());

    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

    public static void sleepFor(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
