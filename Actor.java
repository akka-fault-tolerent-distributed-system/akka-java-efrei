package com.lightbend.akka.sample;

/**
 * TODO:
 * 1 - faire la fonction write
 * 2 - faire la fonction read
 * 3 - implement the onreceive for write and read
 * 4 - create a list of all the acknowledgement for every actor
 * 5 -
 * */

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;

import static com.lightbend.akka.sample.AkkaQuickstart.send;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Actor extends UntypedAbstractActor{

    // Logger attached to actor
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public static int counter = 0;

    private  static int sequence = 0; // write sequence
    private static int Rsequence = 0; // read sequence
    private static String val;
    private static Pair pair;
    public static ArrayList<String> ack_list;

    public static Props createActor() {
        ack_list = new ArrayList<>();
        return Props.create(Actor.class, Actor::new);
    }

    // write function
    // not done
    //TODO: implement waite until more than N/2 [rseq, -, -] receives
    public static Boolean write(String value, ActorRef actorRef) {


        sequence++;

        val = value;

        pair = new Pair(sequence, value);
        send(actorRef, pair);


        try {
            Thread.sleep(1000);
            if (ack_list.size() >= ActorList.l.size() / 2) {
                return true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }

    public static class Pair<X, Y> {
        public final X x;
        public final Y y;
        public Pair(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }

    static public class WelcomeMessage {

        public final String data;

        public WelcomeMessage(String data) {
            this.data = data;
        }
    }

    //TODO: take in the pair when it's received then send an ack
    @Override
    public void onReceive(Object m) throws Throwable {
        if (m instanceof ActorList){
            // go through the list of actors and send a welcome message
            ActorList actorList = (ActorList) m;
            for(ActorRef a : actorList.l){
                a.tell(new WelcomeMessage("Welcome"), getSelf());
            }
        }

        if (m instanceof Pair){
            //if actor receives a Pair then
            Pair pair = (Pair) m;
            // Receive a write change
            if (pair.y instanceof String){
                //send an ack
                getSelf().tell("ack", getSender());
            }
        }

        if(m instanceof String){
            if(m.equals("ack")){
                ack_list.add((String) m);

                log.info("["+getSelf().path().name()+"] write acknowledgement received from["
                        + getSender().path().name() +"] with data: ["+m+"]");
            }
        }

        if (m instanceof WelcomeMessage){
            // print that welcome message received from an actor
            WelcomeMessage me = (WelcomeMessage) m;
            log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+me.data+"]");
            counter++;
        }
    }
}
